let _=require('underscore')
let fs=require('fs')

let flowers = fs.readFileSync('./flower.txt').toString()
let flowers_list = flowers.split("\n")

// console.log(flowers_list)



//1. Count the number of rows.
let rows = _.size(flowers_list)
// console.log(rows)



//2. List the flower name that start with letter 'S'.
let startWithS =_.filter(flowers_list, function(plant){ return plant.charAt(0) == 'S'; });
//console.log(startWithS)



//3. Count the number of flower that not start with letter 'S'.
let withoutS=_.difference(flowers_list, startWithS);
// console.log(withoutS);



//4. List the flower start with first letter of your name if your name start with 'S' use second letter.
let startWithF =_.filter(flowers_list, function(plant){ return plant.charAt(0) == 'F'; });
//console.log(startWithF)



//5. List all the flower the name length is 5.
let fiveLetters = _.filter(flowers_list, function(plant){ return plant.length == 5; });
//console.log(fiveLetters)